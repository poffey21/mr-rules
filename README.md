# MR Rules

How can you set things in the MR (like a label on the MR) and have the merge to master pick up those changes.


The intent of this is to see how easy it is to utilize labels on an MR to manage what gets deployed
from the branch being targeted upon merge.

[View the Use Case on YouTube](https://www.youtube.com/watch?v=h__26LqNe6E)
